package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "github.com/patrickmn/go-cache"
  "time"
)

var inMemCache = cache.New(29*time.Minute, 30*time.Minute)

func SetCache(k string, v interface{}) {
   inMemCache.Set(k, v, cache.DefaultExpiration)
}

func GetCache(k string) (interface{}, bool) {
  return inMemCache.Get(k)
}

func GetCacheItems() map[string]cache.Item {
  return inMemCache.Items()
}

func DeleteCache(k string) {
  inMemCache.Delete(k)
}
