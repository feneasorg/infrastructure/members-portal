package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "fmt"
  "strings"
  "net/http"
  "regexp"

  "github.com/gin-gonic/gin"
  "github.com/gin-contrib/sessions"
)

func GETSettingsHandler(ctx *gin.Context) {
  ctx.HTML(http.StatusOK, "changesettings.html", ViewArgs(ctx, nil))
}

func POSTSettingsHandler(ctx *gin.Context) {
  userInt, _ := ctx.Get("user")
  user, ok := userInt.(*LDAPUser)
  if !ok {
    Redirect(ctx, config.Address, FlashError(ctx, "flash.errors.internal"))
    return
  }

  mailalias := strings.ToLower(ctx.PostForm("mailalias"))
  if _, ok := usernameBlacklist[mailalias]; ok {
    Redirect(ctx, fmt.Sprintf("%s%s", config.Address, "/settings/edit"),
      FlashError(ctx, "form.settings.email.blacklist", mailalias))
    return
  }

  re := regexp.MustCompile(`^[a-z0-9\.]+$`)
  if !re.MatchString(mailalias) {
    Redirect(ctx, fmt.Sprintf("%s%s", config.Address, "/settings/edit"),
      FlashError(ctx, "form.settings.email.regex"))
    return
  }

  err := user.SetEmail(fmt.Sprintf("%s@feneas.org", mailalias))
  if err != nil {
    Redirect(ctx, fmt.Sprintf("%s%s", config.Address, "/settings/edit"),
      FlashError(ctx, "flash.errors.internal"))
    return
  }

  session := sessions.Default(ctx)
  sessionTokenInt := session.Get("Token")
  if sessionToken, ok := sessionTokenInt.(string); ok {
    user.DeleteAllSessionsExcept(sessionToken)
    user.RefreshCache(sessionToken)
  }

  Redirect(ctx, fmt.Sprintf("%s%s", config.Address, "/settings/edit"),
    FlashSuccess(ctx, "form.settings.success"))
}
