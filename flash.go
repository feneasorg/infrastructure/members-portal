package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "github.com/gin-gonic/gin"
)

func FlashSuccess(ctx *gin.Context, message string, args...interface{}) map[string]interface{} {
  return flashMessage(ctx, message, "success", args...)
}

func FlashError(ctx *gin.Context, message string, args...interface{}) map[string]interface{} {
  return flashMessage(ctx, message, "error", args...)
}

func FlashErrors(ctx *gin.Context, args...interface{}) map[string]interface{} {
  return flashMessage(ctx, "", "errors", args...)
}

func flashMessage(ctx *gin.Context, message, messageType string, args...interface{}) map[string]interface{} {
  interfaceType := make(map[string]interface{})
  if messageType == "errors" {
    interfaceType[messageType] = args
    return interfaceType
  }
  interfaceType[messageType] = I18nMessage(ctx, message, args...)
  return map[string]interface{}{"flash": interfaceType}
}
