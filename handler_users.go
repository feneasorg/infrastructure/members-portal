package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "net/http"
  "regexp"

  "github.com/gin-gonic/gin"
  "github.com/gin-contrib/sessions"
  "github.com/dchest/captcha"
)

func GETUsersAboutHandler(ctx *gin.Context) {
  users := make(map[string][]*LDAPUser)
  committee, err := FetchUsersByGroup("committee")
  if err != nil {
    ctx.HTML(http.StatusOK, "index.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.internal")))
    return
  }
  users["committee"] = committee

  admins, err := FetchUsersByGroup("admins")
  if err != nil {
    ctx.HTML(http.StatusOK, "index.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.internal")))
    return
  }
  users["admins"] = admins

  members, err := FetchUsersByGroup("members")
  if err != nil {
    ctx.HTML(http.StatusOK, "index.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.internal")))
    return
  }
  users["members"] = members

  ctx.HTML(http.StatusOK, "aboutus.html",
    ViewArgs(ctx, map[string]interface{}{"users": users}))
}

func GETUsersMapHandler(ctx *gin.Context) {
  var countries Countries
  members, err := FetchUsersByGroup("members")
  if err != nil {
    Redirect(ctx, config.Address, FlashError(ctx, "flash.errors.internal"))
    return
  }

  for _, member := range members {
    country := FindCountryByPostal(member.PostalAddress())
    if country != nil {
      countries = append(countries, *country)
    }
  }

  ctx.HTML(http.StatusOK, "aboutmap.html", ViewArgs(ctx,
    map[string]interface{}{"countries": countries}))
}

func POSTUsersLoginHandler(ctx *gin.Context) {
  username := ctx.PostForm("username")
  password := ctx.PostForm("password")

  user, err := FetchUserByUID(username)
  if err != nil {
    Redirect(ctx, config.Address, FlashError(ctx, "flash.errors.login_failed"))
    return
  }

  if !user.IsValidPassword(password) {
    Redirect(ctx, config.Address, FlashError(ctx, "flash.errors.login_failed"))
    return
  }

  token, err := Token()
  if err != nil {
    Redirect(ctx, config.Address, FlashError(ctx, "flash.errors.internal"))
    return
  }

  SetCache(token, *user)
  // set session cookie
  session := sessions.Default(ctx)
  session.Set("Token", token)
  session.Save()

  Redirect(ctx, config.Address, FlashSuccess(ctx, "login.success"))
}

func GETUsersLogoutHandler(ctx *gin.Context) {
  session := sessions.Default(ctx)
  if token, ok := session.Get("Token").(string); ok {
    // delete it from memory cache
    DeleteCache(token)
  }
  session.Delete("Token")

  Redirect(ctx, config.Address, FlashSuccess(ctx, "logout.success"))
}

func GETUsersRegisterHandler(ctx *gin.Context) {
  ctx.HTML(http.StatusOK, "register.html", ViewArgs(ctx, nil))
}

func POSTUsersRegisterHandler(ctx *gin.Context) {
  var errors []string
  var userData GitlabJob

  captchaID := ctx.PostForm("captchaID")
  captchaValue := ctx.PostForm("captchaValue")

  if !captcha.VerifyString(captchaID, captchaValue) {
    errors = append(errors, I18nMessage(ctx, "flash.errors.captcha"))
  }

  userData.Username = ctx.PostForm("username")
  usernameRegex := regexp.MustCompile(`^[a-z0-9]{3,}$`)
  if !usernameRegex.MatchString(userData.Username) {
    errors = append(errors, I18nMessage(ctx, "flash.errors.register.username"))
  }

  userData.Email = ctx.PostForm("email")
  emailRegex := regexp.MustCompile("^[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[a-zA-Z0-9](?:[\\w-]*[\\w])?$")
  if !emailRegex.MatchString(userData.Email) {
    errors = append(errors, I18nMessage(ctx, "flash.errors.register.email"))
  }

  // XXX that only checks the cache it should check the database instead
  if _, ok := GetCache(userData.Email); ok {
    errors = append(errors, I18nMessage(ctx, "flash.errors.register.exists", userData.Email))
  }

  userData.FirstName = ctx.PostForm("firstName")
  if userData.FirstName == "" {
    errors = append(errors, I18nMessage(ctx, "flash.errors.register.required", "firstName"))
  }

  userData.LastName = ctx.PostForm("lastName")
  if userData.LastName == "" {
    errors = append(errors, I18nMessage(ctx, "flash.errors.register.required", "lastName"))
  }

  userData.PostalAddress = ctx.PostForm("address")
  if userData.PostalAddress == "" {
    errors = append(errors, I18nMessage(ctx, "flash.errors.register.required", "address"))
  }

  userData.SSHPubKey = ctx.PostForm("pubkey")
  userData.Company = ctx.PostForm("company")

  gitlabUsername := ctx.PostForm("gitlabUsername")
  if usernameRegex.MatchString(gitlabUsername) {
    id, err := GitlabID(gitlabUsername)
    if err != nil {
      errors = append(errors,
        I18nMessage(ctx, "flash.errors.register.gitlab", gitlabUsername))
    }
    userData.GitlabID = id
  }

  if len(errors) > 0 {
    ctx.HTML(http.StatusOK, "register.html", ViewArgs(ctx, FlashErrors(ctx, errors)))
    return
  }

  token, err := Token()
  if err != nil {
    ctx.HTML(http.StatusOK, "index.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.internal")))
    return
  }
  SetCache(token, userData)
  SetCache(userData.Email, token)

  // send a mail to the user
  go MailJob{
    To: userData.Email, Lang: "en", // XXX lang hard-coded
    Subject: I18nMessage(ctx, "mail.register.subject"),
    Body: I18nMessage(ctx, "mail.register.body", config.Address, token),
  }.Run()

  ctx.HTML(http.StatusOK, "register.html",
    ViewArgs(ctx, FlashSuccess(ctx, "form.register.submit_success")))
}

func GETUsersVerifyRegistrationHandler(ctx *gin.Context) {
  token := ctx.Param("token")
  userData, ok := GetCache(token)
  registerUser, okUser := userData.(GitlabJob)
  if ok && okUser {
    // trigger gitlab merge request
    go registerUser.Run()
    // clear cache
    DeleteCache(token)
    DeleteCache(registerUser.Email)

    ctx.HTML(http.StatusOK, "verifyregistration.html",
      ViewArgs(ctx, map[string]interface{}{"registerUser": registerUser}))
    return
  }

  Redirect(ctx, config.Address, FlashError(ctx, "flash.errors.expired"))
}
