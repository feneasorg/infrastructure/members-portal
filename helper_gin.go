package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "net/http"
  "strings"
  "time"

  "github.com/gin-gonic/gin"
)

func Redirect(ctx *gin.Context, location string, args map[string]interface{}) {
  if args != nil {
    expires := int(time.Now().Add(time.Second * 60).Unix())
    if flashInt, ok := args["flash"]; ok {
      if flash, ok := flashInt.(map[string]interface{}); ok {
        if message, ok := flash["error"]; ok {
          value, _ := message.(string)
          ctx.SetCookie("FlashErrorMessage", value, expires, "", "", false, false)
        }
        if message, ok := flash["success"]; ok {
          value, _ := message.(string)
          ctx.SetCookie("FlashSuccessMessage", value, expires, "", "", false, false)
        }
      }
    } else {
      if message, ok := args["errors"]; ok {
        value, _ := message.([]string)
        ctx.SetCookie("FlashErrorsMessage", strings.Join(value, ","), expires, "", "", false, false)
      }
    }
  }
  ctx.Redirect(http.StatusFound, location)
}

func ViewArgs(ctx *gin.Context, args map[string]interface{}) map[string]interface{} {
  userInt, _ := ctx.Get("user")
  user, _ := userInt.(*LDAPUser)

  if args == nil {
    args = make(map[string]interface{})
  }

  if message, _ := ctx.Cookie("FlashErrorMessage"); message != "" {
    args["flash"] = map[string]interface{}{"error": message}
    ctx.SetCookie("FlashErrorMessage", "", -1, "", "", false, false)
  }

  if message, _ := ctx.Cookie("FlashErrorsMessage"); message != "" {
    args["errors"] = strings.Split(message, ",")
    ctx.SetCookie("FlashErrorsMessage", "", -1, "", "", false, false)
  }

  if message, _ := ctx.Cookie("FlashSuccessMessage"); message != "" {
    args["flash"] = map[string]interface{}{"success": message}
    ctx.SetCookie("FlashSuccessMessage", "", -1, "", "", false, false)
  }

  if _, ok := args["user"]; !ok {
    args["user"] = user
  }
  return args
}
