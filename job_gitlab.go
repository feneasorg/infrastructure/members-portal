package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "os"
  "fmt"
  "sort"
  "io/ioutil"
  "math/rand"
  "time"

  "gopkg.in/src-d/go-git.v4"
  "gopkg.in/src-d/go-git.v4/plumbing/object"
  "gopkg.in/src-d/go-git.v4/plumbing/transport/http"
  "gopkg.in/src-d/go-git.v4/plumbing"
  "github.com/xanzy/go-gitlab"
  spf13 "github.com/spf13/viper"
)

type GitlabJob struct {
  Username, Email, FirstName, LastName, Company string
  PostalAddress, SSHPubKey                      string
  GitlabID                                      uint
}

func (gitlabJob GitlabJob) Run() {
  if !config.Gitlab.Enabled {
    logger.Debug().Msgf("GitLab integration is disabled. Skipping: %+v", gitlabJob)
    return
  }

  var basicAuth = &http.BasicAuth{
    Username: config.Gitlab.Username,
    Password: config.Gitlab.Password,
  }

  directory, err := ioutil.TempDir("", "register_user")
  if err != nil {
    logger.Error().Err(err).Msg("cannot create temporary directory")
    return
  }
  defer os.RemoveAll(directory)

  // clone or open existing repository
  repository, err := git.PlainClone(directory, false, &git.CloneOptions{
    Auth: basicAuth,
    URL: config.Gitlab.Repository,
    Progress: ioutil.Discard,
  })
  if err != nil {
    logger.Error().Err(err).Msg("cannot clone repository")
    return
  }

  // create a unique branch name and define local and remote refs
  randID := rand.New(rand.NewSource(time.Now().Unix())).Intn(99)
  branch := fmt.Sprintf("%d-add_%s", randID, gitlabJob.Username)
  refName := plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", branch))

  worktree, err := repository.Worktree()
  if err != nil {
    logger.Error().Err(err).Msg("cannot fetch worktree from repository")
    return
  }

  // create new local branch
  err = worktree.Checkout(&git.CheckoutOptions{Branch: refName, Create: true})
  if err != nil {
    logger.Error().Err(err).Msg("cannot checkout worktree")
    return
  }

  // write members/<username>.yml
  viper := spf13.New()
  viper.SetConfigType("yaml")
  viper.Set("external_mail", gitlabJob.Email)
  viper.Set("first_name", gitlabJob.FirstName)
  viper.Set("last_name", gitlabJob.LastName)
  viper.Set("postal_address", gitlabJob.PostalAddress)
  viper.Set("gitlab_id", gitlabJob.GitlabID)
  viper.Set("public_key", gitlabJob.SSHPubKey)
  viper.Set("company", gitlabJob.Company)

  // set default lists and groups
  viper.Set("mailing_lists", []string{config.Gitlab.DefaultMailingList})
  viper.Set("ldap_groups", []string{config.Gitlab.DefaultLdapGroup})

  err = viper.WriteConfigAs(
    fmt.Sprintf("%s/members/%s.yml", directory, gitlabJob.Username))
  if err != nil {
    logger.Error().Err(err).Msg("cannot write yaml file")
    return
  }

  // modify members.yml
  viper = spf13.New()
  viper.SetConfigType("yaml")
  membersFilePath := fmt.Sprintf("%s/members.yml", directory)
  membersFile, err := os.Open(membersFilePath)
  if err != nil {
    logger.Error().Err(err).Msg("cannot open members file")
    return
  }
  defer membersFile.Close()

  err = viper.ReadConfig(membersFile)
  if err != nil {
    logger.Error().Err(err).Msg("cannot read members file")
    return
  }

  currentMembers := append(
    viper.GetStringSlice("current_members"), gitlabJob.Username)
  // sorting the slice or armando will hunt me down
  sort.Strings(currentMembers)
  viper.Set("current_members", currentMembers)

  // save modified file
  err = viper.WriteConfigAs(membersFilePath)
  if err != nil {
    logger.Error().Err(err).Msg("cannot write members file")
    return
  }

  // add all modifications to wt
  _, err = worktree.Add(fmt.Sprintf("members/%s.yml", gitlabJob.Username))
  if err != nil {
    logger.Error().Err(err).Msg("cannot add file to worktree")
    return
  }

  _, err = worktree.Add("members.yml")
  if err != nil {
    logger.Error().Err(err).Msg("cannot add file to worktree")
    return
  }

  // commit worktree
  _, err = worktree.Commit(
    fmt.Sprintf("Add %s", gitlabJob.Username),
    &git.CommitOptions{
      Author: &object.Signature{
        Name:  config.Gitlab.Name,
        Email: config.Gitlab.Email,
        When:  time.Now(),
      },
    },
  )
  if err != nil {
    logger.Error().Err(err).Msg("cannot commit changes")
    return
  }

  // push changes to upstream
  pushOpts := &git.PushOptions{
    Auth: basicAuth,
    Progress: ioutil.Discard,
  }

  err = pushOpts.Validate()
  if err != nil {
    logger.Error().Err(err).Msg("cannot validate push to upstream")
    return
  }

  remote, err := repository.Remote("origin")
  if err != nil {
    logger.Error().Err(err).Msg("cannot find remote")
    return
  }

  err = remote.Push(pushOpts)
  if err != nil {
    logger.Error().Err(err).Msg("cannot push to upstream")
    return
  }

  // finally create a gitlab merge request
  gitlabClient := gitlab.NewClient(nil, config.Gitlab.Password)
  err = gitlabClient.SetBaseURL(config.Gitlab.Url)
  if err != nil {
    logger.Error().Err(err).Msg("cannot set base url")
    return
  }

  var title = fmt.Sprintf("Add %s", gitlabJob.Username)
  var dstBranch = "master"
  var trueBool = true

  mergeRequestOpts := &gitlab.CreateMergeRequestOptions{
    Title: &title,
    SourceBranch: &branch,
    TargetBranch: &dstBranch,
    TargetProjectID: &config.Gitlab.ProjectID,
    RemoveSourceBranch: &trueBool,
    Squash: &trueBool,
  }

  _, _, err = gitlabClient.MergeRequests.CreateMergeRequest(
    config.Gitlab.ProjectID, mergeRequestOpts,
  )
  if err != nil {
    logger.Error().Err(err).Msg("cannot create merge request")
    return
  }
}
