package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "net/http"
  "path/filepath"
  "bytes"

  "github.com/dchest/captcha"
  "github.com/gin-gonic/gin"
)

type captchaRenderer struct {
  Body bytes.Buffer
}

func (renderer captchaRenderer) Render(w http.ResponseWriter) (err error) {
  if renderer.Body.Len() == 0 {
    _, err = w.Write([]byte(":( Something went wrong"))
  } else {
    _, err = w.Write(renderer.Body.Bytes())
  }
  return err
}

func (renderer captchaRenderer) WriteContentType(w http.ResponseWriter) {
  header := w.Header()
  value := header["Content-Type"]

  if len(value) == 0 && renderer.Body.Len() == 0 {
    header["Content-Type"] = []string{"text/plain"}
  } else if len(value) == 0 {
    header["Content-Type"] = []string{"image/png"}
  }
}

func GETCaptchaHandler(ctx *gin.Context) {
  var renderer captchaRenderer

  _, file := filepath.Split(ctx.Param("name"))
  ext := filepath.Ext(file)
  fileEnd := len(file) - len(ext)
  if fileEnd <= 0 {
    logger.Error().Msg("file name is too short")
    ctx.Render(http.StatusInternalServerError, renderer)
    return
  }

  id := file[:len(file)-len(ext)]
  err := captcha.WriteImage(&renderer.Body, id, 250, 100)
  if err != nil {
    logger.Error().Err(err).Msg("cannot create captcha")
    ctx.Render(http.StatusInternalServerError, renderer)
    return
  }

  ctx.Render(http.StatusOK, renderer)
}
