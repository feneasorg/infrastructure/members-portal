package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "strings"
)

func FindCountryByPostal(postal string) *Country {
  postalLower := strings.ToLower(postal)
  for _, country := range config.Countries {
    nameLower := strings.ToLower(country.Name)
    if strings.Contains(postalLower, nameLower) {
      return &country
    } else {
      for _, alias := range country.Aliases {
        aliasLower := strings.ToLower(alias)
        if strings.Contains(postalLower, aliasLower) {
          return &country
        }
      }
    }
  }
  return nil
}
