module git.feneas.org/feneas/infrastructure/members-portal

go 1.13

require (
	github.com/dchest/captcha v0.0.0-20170622155422-6a29415a8364
	github.com/gin-contrib/logger v0.0.2
	github.com/gin-contrib/sessions v0.0.3
	github.com/gin-gonic/contrib v0.0.0-20191209060500-d6e26eeaa607
	github.com/gin-gonic/gin v1.5.0
	github.com/go-gomail/gomail v0.0.0-20160411212932-81ebce5c23df
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/rs/zerolog v1.18.0
	github.com/spf13/viper v1.6.2
	github.com/xanzy/go-gitlab v0.26.0
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df // indirect
	gopkg.in/ldap.v2 v2.5.1
	gopkg.in/src-d/go-git.v4 v4.13.1
)
