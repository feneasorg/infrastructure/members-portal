# Feneas Member Portal

This repository holds the source code of [portal.feneas.org](https://portal.feneas.org)!

## Development

Compile (go version 1.13):

    go build -o portal

Run web-application server:

    ./portal

## Docker

### Run

    docker run --rm -ti -v $(pwd)/config.json:/home/portal/config.json \
      registry.git.feneas.org/feneas/infrastructure/members-portal:latest

### Build

    docker build -t registry.git.feneas.org/feneas/infrastructure/members-portal:latest .

## Packaging

    go build -o portal
    tar -cvzf portal.tgz templates/ public/ messages/ portal config.json.example countries.json
