package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "html/template"
  "crypto/tls"
  "bytes"

  "github.com/go-gomail/gomail"
)

type MailJob struct {
  To, Subject, Body, Lang string
}

func (mail MailJob) Run() {
  if !config.Mail.Enabled {
    logger.Debug().
      Str("to", mail.To).
      Str("subject", mail.Subject).
      Str("body", mail.Body).
      Str("lang", mail.Lang).
      Msg("Mail is disabled. Skipping it!")
    return
  }

  msg := gomail.NewMessage()
  tmpl, err := template.New("mail.html").
    Funcs(templateFuncs).ParseFiles("templates/jobs/mail.html")
  if err != nil {
    logger.Error().Err(err).Msg("cannot parse mail template")
    return
  }

  var buffer bytes.Buffer
  err = tmpl.ExecuteTemplate(&buffer, "mail.html",
    map[string]interface{}{"lang": mail.Lang, "Text": mail.Body})
  if err != nil {
    logger.Error().Err(err).Msg("cannot execute mail template")
    return
  }

  msg.SetHeader("From", config.Mail.From)
  msg.SetHeader("To", mail.To)
  msg.SetHeader("Subject", mail.Subject)
  msg.SetBody("text/html", buffer.String())

  dialer := gomail.NewDialer(config.Mail.Host, config.Mail.Port,
    config.Mail.Username, config.Mail.Password)
  if config.Mail.Insecure {
    dialer.TLSConfig = &tls.Config{InsecureSkipVerify: true}
  }

  err = dialer.DialAndSend(msg)
  if err != nil {
    logger.Error().Err(err).Msg("cannot send mail")
    return
  }
}
