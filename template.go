package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "fmt"
  "strings"
  "html/template"

  "github.com/dchest/captcha"
  "github.com/microcosm-cc/bluemonday"
)

var templateFuncs = template.FuncMap{
  "msg": func(viewArgs map[string]interface{}, message string, args ...interface{}) template.HTML {
    lang, langOK := viewArgs["lang"].(string)
    if !langOK {
      // default to english
      lang = "en"
    }

    dictonary, dictonaryOK := translations[lang]
    if !dictonaryOK {
      // no dictonary found
      return template.HTML(message)
    }

    translation, translationOK := dictonary[message]
    if !translationOK {
      // no translation found
      return template.HTML(message)
    }

    if message, messageOK := translation.(string); messageOK {
      return template.HTML(fmt.Sprintf(message, args...))
    }

    logger.Error().Msgf("cannot cast to string: %+v", translation)
    return template.HTML(message)
  },
  "set": func(viewArgs map[string]interface{}, key string, value interface{}) template.JS {
    viewArgs[key] = value
    return template.JS("")
  },
  "gravatarurl": func(user *LDAPUser) string {
    if user.Gravatar() {
      md5sum := MD5(user.Email())
      if user.PublicEmailField() == "maildrop" {
        md5sum = MD5(user.Maildrop())
      }
      return config.Gravatar.Provider + "/" + md5sum + "?" + config.Gravatar.Params
    }
    return ""
  },
  "config": func() Config { return *config },
  "htmlattr": func(text string) template.HTMLAttr {
    return template.HTMLAttr(text)
  },
  "safehtml": func(text string) template.HTML {
    p := bluemonday.UGCPolicy()
    return template.HTML(p.Sanitize(text))
  },
  "striphtml": func(text string) template.HTML {
    p := bluemonday.StrictPolicy()
    return template.HTML(p.Sanitize(text))
  },
  "captchaid": func() string { return captcha.New() },
  "concat": func(a, b string) string { return a + b },
  "len": func(a []*LDAPUser) int { return len(a) },
  "split": func(a, p string, index int) string {
    result := strings.Split(a, p)
    if len(result) <= index {
      return ""
    }
    return result[index]
  },
  "countriestojson": func(countries Countries) template.JS {
    var result strings.Builder
    var format = `[%f,%f,{
      country:{
        name:"%s",
        iso:"%s"
      }
    }],`

    for _, country := range countries {
      result.WriteString(fmt.Sprintf(
        format, country.Latitude, country.Longitude, country.Name, country.Country))
    }

    return template.JS(fmt.Sprintf("[%s]", strings.TrimRight(result.String(), ",")))
  },
}
