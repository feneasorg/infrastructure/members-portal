package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "fmt"
  "net/http"
  "encoding/json"
  "errors"
)

type gitlabResult struct {
  Id uint `json:"id"`
}

func GitlabID(username string) (uint, error) {
  var result []gitlabResult
  resp, err := http.Get(fmt.Sprintf(
    "%s/api/v4/users?username=%s", config.Gitlab.Url, username))
  if err != nil {
    return 0, err
  }
  defer resp.Body.Close()

  err = json.NewDecoder(resp.Body).Decode(&result)
  if err != nil {
    return 0, err
  }

  if len(result) > 0 {
    return result[0].Id, err
  }
  return 0, errors.New("no gitlab id found")
}
