package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "os"
  "errors"
  "encoding/json"
  "path/filepath"
  "flag"
)

type Config struct {
  Title string `json:"title"`
  Proto string `json:"proto"`
  Address string `json:"address"`
  ListenAddr string `json:"listenAddr"`
  Gitlab struct {
    Enabled bool `json:"enabled"`
    Url string `json:"url"`
    ProjectID int `json:"projectID"`
    Repository string `json:"repository"`
    Username string `json:"username"`
    Password string `json:"password"`
    Directory string `json:"directory"`
    DefaultMailingList string `json:"defaultMailingList"`
    DefaultLdapGroup string `json:"defaultLdapGroup"`
    Name string `json:"name"`
    Email string `json:"email"`
  } `json:"gitlab"`
  Mail struct {
    Enabled bool `json:"enabled"`
    Host string `json:"host"`
    Port int `json:"port"`
    Insecure bool `json:"insecure"`
    Username string `json:"username"`
    Password string `json:"password"`
    From string `json:"from"`
  } `json:"mail"`
  Ldap struct {
    UseSSL bool `json:"useSSL"`
    Username string `json:"username"`
    Password string `json:"password"`
    Port uint `json:"port"`
    Host string `json:"host"`
  } `json:"ldap"`
  Gravatar struct {
    Provider string `json:"provider"`
    Params string `json:"params"`
    DefaultImage string `json:"defaultImage"`
  } `json:"gravatar"`
  Session struct {
    Secret string `json:"secret"`
  } `json:"session"`
  CountryFile string `json:"countryFile"`
  Countries Countries `json:"-"`
}

type Country struct {
  Country string `json:"country"`
  Latitude float64 `json:"latitude"`
  Longitude float64 `json:"longitude"`
  Name string `json:"name"`
  Aliases []string `json:"aliases"`
}

type Countries []Country

var config *Config
var translations = make(map[string]map[string]interface{})

func init() {
  var err error
  var configFile string

  flag.StringVar(&configFile, "config", "config.json",
    "Load configuration from specified file")
  flag.Parse()

  config = &Config{}
  err = decodeJsonFile(configFile, config)
  if err != nil {
    logger.Fatal().Err(err).Msg("cannot decode configuration file")
  }
  logger.Info().Msgf("configuration file %s loaded", configFile)

  err = decodeJsonFile(config.CountryFile, &config.Countries)
  if err != nil {
    logger.Fatal().Err(err).Msg("cannot decode geo country file")
  }
  logger.Info().Msgf("configuration file %s loaded", config.CountryFile)

  if err = filepath.Walk("messages", func(path string, info os.FileInfo, err error) error {
    if err != nil {
      return err
    }

    if !info.IsDir() {
      translation := make(map[string]interface{})
      lang := filepath.Ext(path)
      if len(lang) < 3 {
        return errors.New("file extension is too short")
      }

      file, err := os.Open(path)
      if err != nil {
        return err
      }
      defer file.Close()

      decoder := json.NewDecoder(file)
      err = decoder.Decode(&translation)
      if err != nil {
        return err
      }
      translations[lang[1:]] = translation
      logger.Info().Msgf("translation %s loaded", lang)
    }
    return nil
  }); err != nil {
    logger.Fatal().Err(err).Msg("cannot load translations")
  }
}

func decodeJsonFile(fileName string, object interface{}) error {
  file, err := os.Open(fileName)
  if err != nil {
    return err
  }
  defer file.Close()

  decoder := json.NewDecoder(file)
  err = decoder.Decode(object)
  if err != nil {
    return err
  }
  return nil
}
