package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "os"

  "github.com/rs/zerolog"
  customLog "github.com/gin-contrib/logger"
  "github.com/gin-gonic/contrib/static"
  "github.com/gin-contrib/sessions"
  "github.com/gin-contrib/sessions/cookie"
  "github.com/gin-gonic/gin"
)

var logger = zerolog.New(os.Stdout).
  Output(zerolog.ConsoleWriter{Out: os.Stdout}).
  With().
  Stack().
  Caller().
  Timestamp().
  Logger()

func main() {
  // downgrade zerolog level if gin is in release mode
  if gin.Mode() == "release" {
    zerolog.SetGlobalLevel(zerolog.InfoLevel)
  }

  router := gin.New()
  router.SetFuncMap(templateFuncs)
  router.LoadHTMLGlob("templates/*.html")

  store := cookie.NewStore([]byte(config.Session.Secret))
  router.Use(sessions.Sessions("session", store))
  router.Use(customLog.SetLogger(customLog.Config{Logger: &logger}))

  router.GET("/", authenticationHandler(false), GETIndexHandler)
  userGroup := router.Group("/users", authenticationHandler(false))
  {
    userGroup.POST("/sign_in", POSTUsersLoginHandler)
    userGroup.GET("/sign_out", GETUsersLogoutHandler)
    userGroup.GET("/sign_up", GETUsersRegisterHandler)
    userGroup.POST("/sign_up", POSTUsersRegisterHandler)
    userGroup.GET("/sign_up/:token", GETUsersVerifyRegistrationHandler)
    userGroup.GET("/about", GETUsersAboutHandler)
    userGroup.GET("/map", GETUsersMapHandler)
  }
  profileGroup := router.Group("/profile", authenticationHandler(true))
  {
    profileGroup.GET("/edit", GETChangeProfileHandler)
    profileGroup.POST("/edit", POSTChangeProfileHandler)
  }
  passwordGroup := router.Group("/change_password")
  {
    passwordGroup.GET("", authenticationHandler(true), GETChangePasswordHandler)
    passwordGroup.POST("", authenticationHandler(true), POSTChangePasswordHandler)
    passwordGroup.GET("/:token", GETChangePasswordWithTokenHandler)
    passwordGroup.POST("/:token", POSTChangePasswordWithTokenHandler)
  }
  resetpwGroup := router.Group("/reset_password")
  {
    resetpwGroup.GET("", GETResetPasswordHandler)
    resetpwGroup.POST("", POSTResetPasswordHandler)
  }
  settingsGroup := router.Group("/settings", authenticationHandler(true))
  {
    settingsGroup.GET("/edit", GETSettingsHandler)
    settingsGroup.POST("/edit", POSTSettingsHandler)
  }
  router.GET("/captcha/:name", GETCaptchaHandler)

  router.Use(static.Serve("/public", static.LocalFile("public", true)))

  logger.Info().Msgf("Listening on %s ..", config.ListenAddr)
  logger.Fatal().Err(router.Run(config.ListenAddr)).Msg("cannot bind listening interface")
}
