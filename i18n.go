package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "fmt"

  "github.com/gin-gonic/gin"
)

func I18nMessage(ctx *gin.Context, message string, args...interface{}) string {
  lang := ctx.Query("lang")
  if lang == "" {
    lang = "en" // XXX make default configurable
  }

  dictonary, dictonaryOK := translations[lang]
  if !dictonaryOK {
    // no dictonary found
    return message
  }

  translation, translationOK := dictonary[message]
  if !translationOK {
    // no translation found
    return message
  }

  if message, messageOK := translation.(string); messageOK {
    return fmt.Sprintf(message, args...)
  }

  logger.Error().Msgf("cannot cast to string: %+v", translation)
  return message
}
