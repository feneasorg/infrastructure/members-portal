package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import "github.com/gin-gonic/gin"

func authenticationHandler(blocking bool) gin.HandlerFunc {
  return func(ctx *gin.Context) {
    user, err := CurrentUser(ctx)
    ctx.Set("user", user)

    if blocking && err != nil {
      Redirect(ctx, config.Address, FlashError(ctx, "flash.errors.login_required"))
      ctx.Abort()
    } else {
      ctx.Next()
    }
  }
}
