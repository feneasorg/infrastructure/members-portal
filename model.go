package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "crypto/sha1"
  "errors"
  "strings"
  "crypto/tls"
  "fmt"

  "github.com/gin-gonic/gin"
  "github.com/gin-contrib/sessions"

  ldap "gopkg.in/ldap.v2"
)

func FetchUserByToken(token string) (*LDAPUser, error) {
  if strings.Replace(token, " ", "", -1) == "" {
    return nil, errors.New("Invalid token!")
  }
  entries, err := fetchAttributes(fmt.Sprintf("(&(resetToken=%s))", token))
  if err != nil {
    return nil, err
  }
  return &LDAPUser{entries: entries}, nil
}

func FetchUserByUID(uid string) (*LDAPUser, error) {
  if strings.Replace(uid, " ", "", -1) == "" {
    return nil, errors.New("Invalid UID!")
  }
  entries, err := fetchAttributes(fmt.Sprintf("(&(uid=%s))", uid))
  if err != nil {
    return nil, err
  }
  return &LDAPUser{entries: entries}, nil
}

func FetchUserByEmail(email string) (*LDAPUser, error) {
  if strings.Replace(email, " ", "", -1) == "" {
    return nil, errors.New("Invalid Email!")
  }
  entries, err := fetchAttributes(fmt.Sprintf("(&(maildrop=%s))", email))
  if err != nil {
    return nil, err
  }
  return &LDAPUser{entries: entries}, nil
}

func FetchUsersByGroup(group string) (users []*LDAPUser, err error) {
  entries, err := fetchAttributes(fmt.Sprintf(
    "(memberOf=cn=%s,ou=groups,dc=feneas,dc=org)", group))
  if err != nil {
    return nil, err
  }

  for _, entry := range entries {
    if entry.DN == "cn=admin,dc=feneas,dc=org" {
      // ignore the admin account which is added
      // automatically when the group is created
      continue
    }
    entries := []*ldap.Entry{entry}
    users = append(users, &LDAPUser{entries: entries})
  }
  return
}

func CurrentUser(ctx *gin.Context) (*LDAPUser, error) {
  session := sessions.Default(ctx)
  sessionTokenInt := session.Get("Token")
  if sessionToken, ok := sessionTokenInt.(string); ok {
    userInt, okCache := GetCache(sessionToken)
    userModel, okCast := userInt.(LDAPUser)
    if okCache && okCast {
      return &userModel, nil
    }
  }
  return nil, errors.New("no user found for session")
}

func setAttributes(dn string, attrs map[string][]string) error {
  dial, err := conn()
  if err != nil {
    return err
  }
  defer dial.Close()

  modify := ldap.NewModifyRequest(dn)
  for key, val := range attrs {
    modify.Replace(key, val)
  }

  err = dial.Modify(modify)
  if err != nil {
    return err
  }
  return nil
}

func fetchAttributes(filter string) (entries []*ldap.Entry, err error) {
  dial, err := conn()
  if err != nil {
    return nil, err
  }
  defer dial.Close()

  searchRequest := ldap.NewSearchRequest(
    "dc=feneas,dc=org", ldap.ScopeWholeSubtree, ldap.NeverDerefAliases,
    0, 0, false, filter, []string{"*", "createTimestamp"}, nil,
  )

  search, err := dial.Search(searchRequest)
  if err != nil {
    return entries, err
  }

  entries = search.Entries
  if len(entries) == 0 {
    err = errors.New("LDAP didn't return anything!")
  }
  return
}

func conn() (dial *ldap.Conn, err error) {
  if config.Ldap.UseSSL {
    tlsConfig := &tls.Config{ServerName: config.Ldap.Host}
    dial, err = ldap.DialTLS("tcp", fmt.Sprintf(
      "%s:%d", config.Ldap.Host, config.Ldap.Port), tlsConfig)
  } else {
    dial, err = ldap.Dial("tcp", fmt.Sprintf(
      "%s:%d", config.Ldap.Host, config.Ldap.Port))
  }
  if err != nil {
    return dial, err
  }

  return dial, dial.Bind(config.Ldap.Username, config.Ldap.Password)
}

func createHash(password string, salt []byte) []byte {
  pass := []byte(password)
  str := append(pass[:], salt[:]...)
  sum := sha1.Sum(str)
  result := append(sum[:], salt[:]...)
  return result
}
