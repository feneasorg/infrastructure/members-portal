FROM golang:1.13

# see https://stackoverflow.com/a/36308464
ENV CGO_ENABLED=0

ADD . /go/build
WORKDIR /go/build
RUN go build -o portal

FROM alpine:latest

RUN adduser portal -D -h /home/portal

COPY --from=0 /go/build/messages /home/portal/messages
COPY --from=0 /go/build/public /home/portal/public
COPY --from=0 /go/build/templates /home/portal/templates
COPY --from=0 /go/build/portal /home/portal/portal
COPY --from=0 /go/build/countries.json /home/portal/countries.json
COPY --from=0 /go/build/config.json.example /home/portal/config.json.example

RUN chmod +x /home/portal/portal
RUN chown -R portal:portal /home/portal

USER portal
WORKDIR /home/portal
VOLUME /home/portal/config.json

ENTRYPOINT /home/portal/portal
