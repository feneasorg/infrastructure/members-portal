package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "net/http"

  "github.com/gin-gonic/gin"
  "github.com/gin-contrib/sessions"
  "github.com/dchest/captcha"
)

func GETIndexHandler(ctx *gin.Context) {
  ctx.HTML(http.StatusOK, "index.html", ViewArgs(ctx, nil))
}

func GETChangePasswordHandler(ctx *gin.Context) {
  ctx.HTML(http.StatusOK, "changepassword.html", ViewArgs(ctx, nil))
}

func POSTChangePasswordHandler(ctx *gin.Context) {
  userInt, _ := ctx.Get("user")
  user, ok := userInt.(*LDAPUser)
  if !ok {
    Redirect(ctx, config.Address, FlashError(ctx, "flash.errors.internal"))
    return
  }

  oldPassword := ctx.PostForm("oldpassword")
  password := ctx.PostForm("password")
  retype := ctx.PostForm("retype")

  if user.IsValidPassword(oldPassword) {
    if password != retype {
      ctx.HTML(http.StatusOK, "changepassword.html",
        ViewArgs(ctx, FlashError(ctx, "flash.errors.password.retype")))
      return
    }

    err := user.ChangePassword(password)
    if err != nil {
      ctx.HTML(http.StatusOK, "changepassword.html",
        ViewArgs(ctx, FlashError(ctx, "flash.errors.internal")))
      return
    }

    session := sessions.Default(ctx)
    sessionTokenInt := session.Get("Token")
    if sessionToken, ok := sessionTokenInt.(string); ok {
      user.DeleteAllSessionsExcept(sessionToken)
      user.RefreshCache(sessionToken)
    }

    Redirect(ctx, config.Address, FlashSuccess(ctx, "form.success"))
    return
  } else {
    Redirect(ctx, config.Address, FlashError(ctx, "flash.errors.wrong_pw"))
    return
  }
}

func GETChangePasswordWithTokenHandler(ctx *gin.Context) {
  token := ctx.Param("token")
  user, err := FetchUserByToken(token)
  if err != nil {
    ctx.HTML(http.StatusOK, "index.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.expired")))
    return
  }

  if !user.IsValidToken(token) {
    ctx.HTML(http.StatusOK, "index.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.expired")))
    return
  }

  ctx.HTML(http.StatusOK, "changepassword.html",
    ViewArgs(ctx, map[string]interface{}{"token": token}))
}

func POSTChangePasswordWithTokenHandler(ctx *gin.Context) {
  token := ctx.Param("token")
  user, err := FetchUserByToken(token)
  if err != nil {
    Redirect(ctx, config.Address, FlashError(ctx, "flash.errors.expired"))
    return
  }

  if !user.IsValidToken(token) {
    Redirect(ctx, config.Address, FlashError(ctx, "flash.errors.expired"))
    return
  }

  password := ctx.PostForm("password")
  retype := ctx.PostForm("retype")

  if password != retype {
    args := FlashError(ctx, "flash.errors.password.retype")
    args["token"] = token
    ctx.HTML(http.StatusOK, "changepassword.html", ViewArgs(ctx, args))
    return
  }

  // delete token it is a one-time thing only
  err = user.SetResetToken(nil)
  if err != nil {
    args := FlashError(ctx, "flash.errors.internal")
    args["token"] = token
    ctx.HTML(http.StatusOK, "changepassword.html", ViewArgs(ctx, args))
    return
  }

  err = user.ChangePassword(password)
  if err != nil {
    args := FlashError(ctx, "flash.errors.internal")
    args["token"] = token
    ctx.HTML(http.StatusOK, "changepassword.html", ViewArgs(ctx, args))
    return
  }

  Redirect(ctx, config.Address, FlashSuccess(ctx, "form.success"))
}

func GETResetPasswordHandler(ctx *gin.Context) {
  ctx.HTML(http.StatusOK, "resetpassword.html", ViewArgs(ctx, nil))
}

func POSTResetPasswordHandler(ctx *gin.Context) {
  email := ctx.PostForm("email")
  captchaID := ctx.PostForm("captchaID")
  captchaValue := ctx.PostForm("captchaValue")

  if !captcha.VerifyString(captchaID, captchaValue) {
    ctx.HTML(http.StatusOK, "resetpassword.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.captcha")))
    return
  }

  user, err := FetchUserByEmail(email)
  if err != nil {
    // don't reveal if mail address is in use see #4
    ctx.HTML(http.StatusOK, "resetpassword.html",
      ViewArgs(ctx, FlashSuccess(ctx, "flash.reset_password.success")))
    return
  }

  token, err := Token()
  if err != nil {
    ctx.HTML(http.StatusOK, "resetpassword.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.internal")))
    return
  }

  err = user.SetResetToken(&token)
  if err != nil {
    ctx.HTML(http.StatusOK, "resetpassword.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.internal")))
    return
  }

  // send a mail to the user
  go MailJob{
    To: email, Lang: "en",
    Subject: I18nMessage(ctx, "mail.reset_password.subject"),
    Body: I18nMessage(ctx, "mail.reset_password.body", config.Address, token),
  }.Run()

  ctx.HTML(http.StatusOK, "resetpassword.html",
    ViewArgs(ctx, FlashSuccess(ctx, "form.reset_password.success")))
}
