package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "regexp"
  "net/http"
  "fmt"

  "github.com/gin-gonic/gin"
  "github.com/gin-contrib/sessions"
)

func GETChangeProfileHandler(ctx *gin.Context) {
  ctx.HTML(http.StatusOK, "changeprofile.html", ViewArgs(ctx, nil))
}

func POSTChangeProfileHandler(ctx *gin.Context) {
  userInt, _ := ctx.Get("user")
  user, ok := userInt.(*LDAPUser)
  if !ok {
    Redirect(ctx, config.Address, FlashError(ctx, "flash.errors.internal"))
    return
  }

  description := ctx.PostForm("description")
  emailField := ctx.PostForm("emailField")
  public := ctx.PostForm("public") != ""
  gravatar := ctx.PostForm("gravatar") != ""

  if len(description) > 400 {
    ctx.HTML(http.StatusOK, "changeprofile.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.validation.profile.description")))
    return
  }

  re := regexp.MustCompile("^(mail|maildrop)$")
  if !re.MatchString(emailField) {
    ctx.HTML(http.StatusOK, "changeprofile.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.validation.profile.email_field")))
    return
  }

  err := user.SetDescription(description)
  if err != nil {
    ctx.HTML(http.StatusOK, "changeprofile.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.internal")))
    return
  }

  err = user.SetPublicProfile(public)
  if err != nil {
    ctx.HTML(http.StatusOK, "changeprofile.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.internal")))
    return
  }

  err = user.SetGravatar(gravatar)
  if err != nil {
    ctx.HTML(http.StatusOK, "changeprofile.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.internal")))
    return
  }

  err = user.SetPublicEmailField(emailField)
  if err != nil {
    ctx.HTML(http.StatusOK, "changeprofile.html",
      ViewArgs(ctx, FlashError(ctx, "flash.errors.internal")))
    return
  }

  session := sessions.Default(ctx)
  sessionTokenInt := session.Get("Token")
  if sessionToken, ok := sessionTokenInt.(string); ok {
    user.DeleteAllSessionsExcept(sessionToken)
    user.RefreshCache(sessionToken)
  }

  Redirect(ctx, fmt.Sprintf("%s%s", config.Address, "/profile/edit"), nil)
}
