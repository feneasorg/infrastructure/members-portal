package main
//
// Feneas Member Portal
// Copyright (C) 2020 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "encoding/base64"
  "fmt"
  "errors"
  "time"
  "strings"

  ldap "gopkg.in/ldap.v2"
  "github.com/microcosm-cc/bluemonday"
)

type LDAPUser struct {
  entries []*ldap.Entry
}

func (user *LDAPUser) String() (result string) {
  for i, entry := range user.entries {
    result = fmt.Sprintf("%s\n#%d: DN %s", result, i, entry.DN)
    for j, attr := range entry.Attributes {
      result = fmt.Sprintf("%s\n#%d.%d: Name(%s) Values(%s)",
        result, i, j, attr.Name, attr.Values)
    }
  }
  return
}

func (user *LDAPUser) DN() (string, error) {
  for _, entry := range user.entries {
    uid := entry.GetAttributeValue("uid")
    if uid != "" {
      return entry.DN, nil
    }
  }
  return "", errors.New("No matching attribute found!")
}

func (user *LDAPUser) Organization() string {
  orga, _ := user.getAttribute("o")
  return orga
}

func (user *LDAPUser) PostalAddress() string {
  address, _ := user.getAttribute("postalAddress")
  return address
}

func (user *LDAPUser) Email() string {
  mail, err := user.getAttribute("mail")
  if err != nil {
    return ""
  }
  return mail
}

func (user *LDAPUser) SetEmail(mail string) error {
  dn, err := user.DN()
  if err != nil {
    return err
  }

  attrs := make(map[string][]string)
  attrs["mail"] = []string{mail}
  return setAttributes(dn, attrs)
}

func (user *LDAPUser) Maildrop() string {
  mail, err := user.getAttribute("maildrop")
  if err != nil {
    return ""
  }
  return mail
}

func (user *LDAPUser) Description() string {
  desc, err := user.getAttribute("description")
  if err != nil {
    return ""
  }
  return desc
}

func (user *LDAPUser) SetDescription(s string) error {
  dn, err := user.DN()
  if err != nil {
    return err
  }

  // fix for feneas/infrastructure/members-portal#13
  if s == "" { s = " " }

  p := bluemonday.UGCPolicy()
  attrs := make(map[string][]string)
  attrs["description"] = []string{p.Sanitize(s)}
  return setAttributes(dn, attrs)
}

func (user *LDAPUser) Gravatar() bool {
  public, err := user.getAttribute("gravatar")
  return err == nil && public == "TRUE"
}

func (user *LDAPUser) SetGravatar(enabled bool) error {
  dn, err := user.DN()
  if err != nil {
    return err
  }

  gravatar := "FALSE"
  if enabled {
    gravatar = "TRUE"
  }

  attrs := make(map[string][]string)
  attrs["gravatar"] = []string{gravatar}
  return setAttributes(dn, attrs)
}

func (user *LDAPUser) PublicEmailField() string {
  field, err := user.getAttribute("publicEmailField")
  if err != nil {
    return "mail"
  }
  return field
}

func (user *LDAPUser) SetPublicEmailField(field string) error {
  dn, err := user.DN()
  if err != nil {
    return err
  }

  attrs := make(map[string][]string)
  attrs["publicEmailField"] = []string{field}
  return setAttributes(dn, attrs)
}

func (user *LDAPUser) PublicProfile() bool {
  public, err := user.getAttribute("publicProfile")
  return err == nil && public == "TRUE"
}

func (user *LDAPUser) SetPublicProfile(public bool) error {
  dn, err := user.DN()
  if err != nil {
    return err
  }

  publicProfile := "FALSE"
  if public {
    publicProfile = "TRUE"
  }

  attrs := make(map[string][]string)
  attrs["publicProfile"] = []string{publicProfile}
  return setAttributes(dn, attrs)
}

func (user *LDAPUser) CN() (string, error) {
  return user.getAttribute("cn")
}

func (user *LDAPUser) UID() (string, error) {
  return user.getAttribute("uid")
}

func (user *LDAPUser) Password() (string, error) {
  return user.getAttribute("userPassword")
}

func (user *LDAPUser) NextcloudQuota() (string, error) {
  return user.getAttribute("nextcloudQuota")
}

func (user *LDAPUser) CreatedAt() (time.Time, error) {
  return user.getTime("createTimestamp")
}

func (user *LDAPUser) ResetTokenCreatedAt() (time.Time, error) {
  return user.getTime("resetTimestamp")
}

func (user *LDAPUser) getTime(attr string) (time.Time, error) {
  stringAttr, err := user.getAttribute(attr)
  if err != nil {
    return time.Time{}, err
  }
  return time.Parse("20060102150405Z", stringAttr)
}

func (user *LDAPUser) ResetToken() (string, error) {
  return user.getAttribute("resetToken")
}

func (user *LDAPUser) getAttribute(attr string) (string, error) {
  for _, entry := range user.entries {
    value := entry.GetAttributeValue(attr)
    if value != "" {
      return value, nil
    }
  }
  return "", errors.New("No matching attribute found!")
}

func (user *LDAPUser) IsValidPassword(password string) bool {
  hash, err := user.Password()
  if err != nil {
    return false
  }

  // currently we only support SSHA
  if len(hash) < 7 || string(hash[0:6]) != "{SSHA}" {
    return false
  }

  data, err := base64.StdEncoding.DecodeString(hash[6:])
  if len(data) < 21 || err != nil {
    return false
  }

  newhash := createHash(password, data[20:])
  hashedpw := base64.StdEncoding.EncodeToString(newhash)

  if hashedpw == hash[6:] {
    return true
  }
  return false
}

func (user *LDAPUser) IsValidToken(token string) bool {
  if strings.Replace(token, " ", "", -1) == "" {
    // token empty
    return false
  }

  oneWeekAgo := time.Now().Add(time.Hour * 24 * 3 * -1)
  ldapToken, err := user.ResetToken()
  if err != nil {
    logger.Debug().Err(err).Msg("cannot reset token")
    return false
  }

  created, err := user.ResetTokenCreatedAt()
  if err != nil {
    logger.Debug().Err(err).Msg("cannot fetch reset token timestamp")
    return false
  }

  // if token matches and is not expired then render
  if ldapToken == token && oneWeekAgo.Before(created) {
    return true
  }
  return false
}

func (user *LDAPUser) ChangePassword(password string) (err error) {
  dial, err := conn()
  if err != nil {
    return err
  }
  defer dial.Close()

  dn, err := user.DN()
  if err != nil {
    return err
  }

  modify := ldap.NewPasswordModifyRequest(dn, "", password)
  _, err = dial.PasswordModify(modify)
  if err != nil {
    return err
  }
  return nil
}

func (user *LDAPUser) SetResetToken(token *string) (err error) {
  dial, err := conn()
  if err != nil {
    return err
  }
  defer dial.Close()

  dn, err := user.DN()
  if err != nil {
    return err
  }

  modify := ldap.NewModifyRequest(dn)
  if token == nil {
    modify.Delete("resetToken", nil)
  } else {
    modify.Replace("resetTimestamp", []string{
      time.Now().Format("20060102150405Z")})
    modify.Replace("resetToken", []string{*token})
  }
  return dial.Modify(modify)
}

func (user *LDAPUser) DeleteAllSessionsExcept(session string) {
  for key, item := range GetCacheItems() {
    if key == session {
      continue
    }

    if sessionUser, ok := item.Object.(LDAPUser); ok {
      sessionDN, err := sessionUser.DN()
      if err != nil {
        logger.Warn().Err(err).Msg("cannot fetch user session DN")
        continue
      }

      dn, err := user.DN()
      if err != nil {
        logger.Warn().Err(err).Msg("cannot fetch user DN")
        continue
      }

      if dn == sessionDN {
        DeleteCache(key)
      }
    }
  }
}

func (user *LDAPUser) RefreshCache(session string) error {
  uid, err := user.UID()
  if err != nil {
    return err
  }

  user, err = FetchUserByUID(uid)
  if err != nil {
    return err
  }

  SetCache(session, *user)
  return nil
}
